/** @format */

import React from 'react';
import { Image, Button, Alert } from 'react-native';
//import { createAppContainer } from 'react-navigation'; //v4
import { NavigationContainer } from '@react-navigation/native';

//import { createStackNavigator } from 'react-navigation-stack'; //v4
import { createStackNavigator } from '@react-navigation/stack';

import AddPinScreen from '../screens/AddPinScreen';
import GalleryScreen from '../screens/GalleryScreen';
import HomeScreen from '../screens/HomeScreen';
import SearchTagsScreen from '../screens/SearchTagsScreen';
import AddTagsScreen from '../screens/AddTagsScreen';

import ClockScreen_Test from '../screens/Test/ClockScreen_Test';
import ImagePicScreen_Test from '../screens/Test/ImagePicScreen_Test';
import GalleryTestScreen_Test from '../screens/Test/GalleryTestScreen_Test';

import tcolors from '../constants/ThemeColors';
import def from '../constants/DefaultStyles';

import { latitude_m, longitude_m } from '../screens/AddPinScreen';

const Stack = createStackNavigator();

function LogoTitle() {
	return (
		<Image
			style={{ width: 133, height: 33 }}
			source={require('../assets/Rove_logo_wht.png')}
		/>
	);
}

export default function RoveNavigation() {
	//const RoveNavigator = createStackNavigator({ //v4
	return (
		<NavigationContainer>
			<Stack.Navigator
				initialRouteName="Home" //'Home' SearchTest
				screenOptions={{
					gestureEnabled: false,
					headerStyle: {
						backgroundColor: tcolors.BckgdColor,
					},
					headerTintColor: 'white',
					headerTitleStyle: {
						textAlign: 'center',
						fontFamily: 'monMed',
						fontSize: 28,
						color: 'white',
					},
				}}
			>
				<Stack.Screen
					name="Clock_Test"
					component={ClockScreen_Test}
					options={({ navigation, props }) => ({
						headerTitle:'Clock Test',
					})}
					/>
					<Stack.Screen
					name="Image_Test"
					component={ImagePicScreen_Test}
					options={({ navigation, props }) => ({
						headerTitle:'Image Picker Test',
					})}
					/>
					<Stack.Screen
					name="Gallery_Test"
					component={GalleryTestScreen_Test}
					options={({ navigation, props }) => ({
						headerTitle:'Gallery Test',
					})}
					/>
				<Stack.Screen
					name="Home"
					component={HomeScreen}
					options={({ navigation, props }) => ({
						headerTitle: <LogoTitle {...props} />,
						headerRight: () => (
							<Button
								title="Search"
								onPress={() =>
									navigation.navigate('SearchTags')
								}
								color="white"
							/>
						),
					})}
				/>
				<Stack.Screen
					name="AddPin"
					component={AddPinScreen}
					options={({ navigation, route }) => ({
						headerTitle: 'Add Pin Test',
						headerRight: () => (
							<Button
								title="Add Tags"
								onPress={() => {
									if (false==false) {
										Alert.alert( 'Add a Pin','Press and hold to add a pin to the map',
											[{text: 'OK', onPress: () => console.log( 'OK Pressed')}],{ cancelable: false }
										);
									} else navigation.navigate('AddTags');
								}}
								color="white"
							/>
						),
					})}
				/>
				<Stack.Screen
					name="AddTags"
					component={AddTagsScreen}
					options={
						({ title: 'Add Tags' },
						({ navigation, route }) => ({
							headerRight: () => (
								<Button
									title="Gallery"
									onPress={() =>
										navigation.navigate('Gallery')
									}
									color="white"
								/>
							),
						}))
					}
				/>
				<Stack.Screen
					name="Gallery"
					component={GalleryScreen}
					options={
						({ title: 'Gallery' },
						({ navigation }) => ({
							headerRight: () => (
								<Button
									title="Home"
									onPress={() =>
										navigation.navigate('Home')
									}
									color="white"
								/>
							),
						}))
					}
				/>
				<Stack.Screen
					name="SearchTags"
					component={SearchTagsScreen}
					options={
						({ title: 'Search Tags' },
						({ navigation }) => ({
							headerRight: () => (
								<Button
									title="Home"
									onPress={() => {
										/* 1. Navigate to the Details route with params */
										navigation.navigate('Home');
									}}
									color="white"
								/>
							),
						}))
					}
				/>
			</Stack.Navigator>
		</NavigationContainer>
	);
}
//export default RoveNavigation;
//export default createStackNavigator(RoveNavigator);
