/** @format */
import { gestureHandlerRootHOC } from 'react-native-gesture-handler'; //React Nav. v5
// must be first import in App.js without it app may crash
// in production even if it works fine in development.

import React, { useState } from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';

import * as Font from 'expo-font';
//import { AppLoading } from 'expo';
import AppLoading from 'expo-app-loading';

import RoveNavigator from './navigation/RoveNavigator';

const fetchFonts = () => {
	return Font.loadAsync({
		monLite: require('./assets/fonts/Montserrat-Light.ttf'),
		monReg: require('./assets/fonts/Montserrat-Regular.ttf'),
		monMed: require('./assets/fonts/Montserrat-Medium.ttf'),
		monSemiBold: require('./assets/fonts/Montserrat-SemiBold.ttf'),
	});
};

export default function App() {
	const [fontLoaded, setFontLoaded] = useState(false);

	if (!fontLoaded) {
		return (
			<AppLoading
				startAsync={fetchFonts}
				onError={console.warn}
				onFinish={() => setFontLoaded(true)}
			/>
		);
	}
	return <RoveNavigator/>;
}
