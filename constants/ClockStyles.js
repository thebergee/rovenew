import { StyleSheet, Dimensions } from 'react-native';

const w = (Dimensions.get('screen').width)*.75;
const fntSize=16;

export default StyleSheet.create({
	screen: {
		flex: 1,
		//backgroundColor:'chartreuse',
	},
	
	tempContainer:{
		marginVertical:10,
		flexDirection:'row'
	},

	itemContainer: {
		backgroundColor: 'orange',
		position: 'relative',
		flexDirection: 'row',
		alignContent: 'center',
		justifyContent: 'space-evenly',
		flexWrap: 'wrap',
		marginTop: 8.5,
		marginBottom: 8.5,
		backgroundColor:'transparent'
	},

    mainText:{
        fontFamily:'monMed',
        fontSize: 30,
        textAlign:'center'
	},

    circle: {
		width: w,
		height: w,
		borderRadius: w / 2,
		overflow: 'hidden',
		alignContent: 'center',
		justifyContent: 'center',
        alignItems: 'center',
        backgroundColor:'deepskyblue',
	},

    pacman: {
		width: 0,
		height: 0,
		borderTopWidth: 60,
		borderTopColor: 'yellow',
		borderLeftColor: 'yellow',
		borderLeftWidth: 60,
		borderRightColor: 'transparent',
		borderRightWidth: 60,
		borderBottomColor: 'yellow',
		borderBottomWidth: 60,
		borderTopLeftRadius: 60,
		borderTopRightRadius: 60,
		borderBottomRightRadius: 60,
		borderBottomLeftRadius: 60,
	},
	buttonA: {
		paddingVertical: 10,
		paddingHorizontal: 22,
		marginBottom: 8.5,
		borderRadius: 40,
		overflow: 'hidden',
		alignContent: 'center',
		justifyContent:'center'
	},
	bodyText:{
		fontFamily: 'monMed',
		fontSize: fntSize,
		color: 'black',
		textAlign:'left'
	},
	inputName:{
		fontFamily: 'monMed',
		fontSize: fntSize,
		color: 'grey',
		textAlign:'left',
		borderBottomWidth:2,
		width:200
	},
	tempInput:{
		fontFamily: 'monMed',
		fontSize: fntSize,
		color: 'grey',
		textAlign:'center',
		borderBottomWidth:2,
		width:50,
		marginLeft:10
	}
});