import { StyleSheet, Dimensions } from 'react-native';

import tcolors from './ThemeColors';

export default StyleSheet.create({
	//used in AddTags and in SearchTags Screen
	labels: {
		color: '#4E4E4F',
		fontFamily: 'monMed',
		fontSize: 22,
	},
	buffer: {
		position: 'relative',
		marginTop: 5,
		marginBottom: 5,
		height: 10,
		//backgroundColor: 'chartreuse',
	},

	searchCon: {
		//backgroundColor: 'green',
		marginLeft: 10,
		marginRight: 10,
	},
	
	tagsContainer: {
		//backgroundColor: 'orange',
		position: 'relative',
		flexDirection: 'row',
		alignContent: 'center',
		justifyContent: 'space-evenly',
		flexWrap: 'wrap',
		marginTop: 8.5,
		marginBottom: 8.5,
	},

	//MapMarker,SubTag
	button: {
		paddingVertical: 10,
		paddingHorizontal: 22,
		marginBottom: 8.5,
		borderRadius: 40,
		overflow: 'hidden',
		alignContent: 'center',
		justifyContent: 'center',
		alignItems: 'center',
	},
	superbutton: {
		paddingVertical: 10,
		paddingHorizontal: 25,
		borderRadius: 40,
		overflow: 'hidden',
		marginVertical: 8.5,
		alignContent: 'center',
		justifyContent: 'center',
		alignItems: 'center',
	},

	buttonText: {
		fontFamily: 'monMed',
		fontSize: 18,
		color: '#fff',
        textAlign: 'center',
        textAlignVertical:'center',
		//backgroundColor: 'mediumpurple',
	},
});
