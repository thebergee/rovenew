/** @format */

import { StyleSheet, Dimensions } from 'react-native';

import tcolors from './ThemeColors';
const w = 80;

export default StyleSheet.create({
	//Gallery Screen layout
	galleryScreen: {
		position: 'relative',
		flexDirection: 'row',
		flexWrap: 'wrap',
		backgroundColor: 'black'
	},
	scrollGallery:{
		backgroundColor: 'black'
	},
	//used on HomeScreen
	addButtonContainer: {
		position: 'absolute',
		top: Dimensions.get('screen').height * 0.73,
		left: Dimensions.get('screen').width * 0.05,
	},

	//Container that holds the AddButton to add a new Image
	addImageContainer: {
		justifyContent: 'center',
		alignItems: 'center',
		borderColor: 'white',
		borderWidth: 5,
		borderStyle: 'dotted',
		alignContent: 'center',
		width: Dimensions.get('screen').width * 0.5,
		height: Dimensions.get('screen').height * 0.25,
	},

	//image frame to hold the gallery images
	imageFrame: {
		overflow: 'hidden', //cover or hidden
		justifyContent: 'center',
		backgroundColor: 'chartreuse',
		borderColor: 'dodgerblue',
		//borderWidth: 10,
		alignContent: 'center',
		width: Dimensions.get('screen').width * 0.5,
		height: Dimensions.get('screen').height * 0.25,
	},

	circle: {
		width: w,
		height: w,
		borderRadius: w / 2,
		overflow: 'hidden',
		alignContent: 'center',
		justifyContent: 'center',
		alignItems: 'center',
		backgroundColor: 'deeppink',
	},

	buffer: {
		position: 'relative',
		marginTop: 5,
		marginBottom: 5,
		height: 10,
		backgroundColor: 'chartreuse',
	},

	image: { width: '100%', height: '100%' },
});
