/** @format */

export default {
	BckgdColor: '#1d1d27', //for header backgrounds
	galleryAdd: '#0A89C2',

	//linear gradient secondaryA to secondaryB
	secondaryA: '#46d2fd',
	secondaryB: '#5351f0',
	secondaryC: '#666372', //inactive toggle button
};
