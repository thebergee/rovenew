/** @format */

import { StyleSheet, Dimensions } from 'react-native';

import tcolors from './ThemeColors';
const w = 80;

export default StyleSheet.create({
	screen: {
		flex: 1,
		//backgroundColor:'chartreuse',
	},

	//used on HomeScreen
	addButtonContainer: {
		position: 'absolute',
		top: Dimensions.get('screen').height * 0.73,
		left: Dimensions.get('screen').width * 0.05,
	},

	//Used for testing
	testButtonContainer: {
		flexDirection: 'row',
		zIndex: 50,
		backgroundColor: 'plum',
		alignItems: 'center',
		alignContent: 'center',
	},

	//Used on HomeScreen and AddPinScreen
	map: {
		...StyleSheet.absoluteFillObject,
	},

	//wireframe text
	wfText: {
		//wireframe text
		fontFamily: 'monReg',
		textAlign: 'center',
		fontSize: 20,
	},

	//Consistent with the headerTitleStyle for RoveNavigator
	headerTitleStyle: {
		textAlign: 'center',
		fontFamily: 'monMed',
		fontSize: 28,
		color: 'white',
	}
});
