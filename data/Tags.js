/* Super Pins */
//Other, City, Rural, Coastal, Wilderness
var superTags = [
	{
		title: 'Other',
		id: 'sp1',
		active: false,
	},
	{
		title: 'City',
		id: 'sp2',
		active: false,
	},
	{
		title: 'Rural',
		id: 'sp3',
		active: false,
	},
	{
		title: 'Coastal',
		id: 'sp4',
		active: false,
	},
	{
		title: 'Wilderness',
		id: 'sp5',
		active: false,
	},
];

/* Sub Pins */
// Architecture, Landmark, Beach, Graffitti, Wildlife,
// Transport, Abandoned, Entertainment, Commercial, Residential,
var subTags = [
	{
		title: 'Architecture',
		id: 'sb1',
		active: false,
	},
	{
		title: 'Landmark',
		id: 'sb2',
		active: false,
	},
	{
		title: 'Beach',
		id: 'sb3',
		active: false,
	},
	{
		title: 'Graffiti',
		id: 'sb4',
		active: false,
	},
	{
		title: 'Wildlife',
		id: 'sb5',
		active: false,
	},
	{
		title: 'Transport',
		id: 'sb6',
		active: false,
	},
	{
		title: 'Abandoned',
		id: 'sb7',
		active: false,
	},
	{
		title: 'Entertainment',
		id: 'sb8',
		active: false,
	},
	{
		title: 'Commercial',
		id: 'sb9',
		active: false,
	},
	{
		title: 'Residential',
		id: 'sb10',
		active: false,
	},
];

export {subTags,superTags};