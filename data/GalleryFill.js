/* Super Pins */
//Other, City, Rural, Coastal, Wilderness

/* Sub Pins */
// Architecture, Landmark, Beach, Graffitti, Wildlife,
// Transport, Abandoned, Entertainment, Commercial, Residential,

//City
const FILL_1 = [
	'https://assets.gamepur.com/wp-content/uploads/2020/03/17233304/Cyberpunk-2077-main.jpg',
	'https://i.pinimg.com/originals/06/18/25/061825793972742302cfd9294f4e53aa.jpg',
	'https://i.pinimg.com/originals/77/ed/d8/77edd8e59249472acd76ebec0358e97f.png',
	'https://www.natickma.gov/ImageRepository/Document?documentID=1251',
	'http://cdn.cnn.com/cnnnext/dam/assets/190910120101-04-shopping-cities-photos.jpg',
	'https://www.t-systems.com/resource/image/159554/ratio3x4/768/1024/7cdefba5640b2a18ea3d34adc6009e06/rZ/m-smart-city.jpg',
	'https://www.brookings.edu/wp-content/uploads/2016/04/newyork_skyline_modern_city.jpg',
];

//Night City
const FILL_2 = [
	'https://twinfinite.net/wp-content/uploads/2020/04/cyberpunk-city-center-2.jpg',
	'https://thenewswheel.com/wp-content/uploads/2020/08/City-Highway-Timelapse-1000x750.jpg',
	'https://i.pinimg.com/originals/5d/a2/2d/5da22d8b4f6479002e9dfc27bd0d89a9.jpg',
	'https://www.domusweb.it/content/dam/domusweb/it/news/gallery/2020/12/21/night-city-cyberpunk-2077/gallery/Cyberpunk2077_The_market_RGB.png.foto.rmedium.png',
	'https://d1lss44hh2trtw.cloudfront.net/assets/article/2020/06/25/cyberpunk-2077-trailer_feature.jpg',
	'https://sm.ign.com/ign_nordic/screenshot/default/e32019-xbox-cyberpunk-2077-screenshot-12-1560112102315_z6bt.jpg',
	'https://assets1.ignimgs.com/2020/06/25/ebx2nwlxqaeacmt-1593105281705.jpeg',
];

const FILL_3 = [
	'https://cdn.pixabay.com/photo/2016/05/05/23/52/mountain-summit-1375015_960_720.jpg',
	'https://twinfinite.net/wp-content/uploads/2020/04/cyberpunk-city-center-2.jpg',
	'https://thenewswheel.com/wp-content/uploads/2020/08/City-Highway-Timelapse-1000x750.jpg',
	'https://i.pinimg.com/originals/5d/a2/2d/5da22d8b4f6479002e9dfc27bd0d89a9.jpg',
	'https://www.domusweb.it/content/dam/domusweb/it/news/gallery/2020/12/21/night-city-cyberpunk-2077/gallery/Cyberpunk2077_The_market_RGB.png.foto.rmedium.png',
	'https://d1lss44hh2trtw.cloudfront.net/assets/article/2020/06/25/cyberpunk-2077-trailer_feature.jpg',
	'https://sm.ign.com/ign_nordic/screenshot/default/e32019-xbox-cyberpunk-2077-screenshot-12-1560112102315_z6bt.jpg',
	'https://assets1.ignimgs.com/2020/06/25/ebx2nwlxqaeacmt-1593105281705.jpeg',
	'https://assets.gamepur.com/wp-content/uploads/2020/03/17233304/Cyberpunk-2077-main.jpg',
	'https://i.pinimg.com/originals/06/18/25/061825793972742302cfd9294f4e53aa.jpg',
	'https://i.pinimg.com/originals/77/ed/d8/77edd8e59249472acd76ebec0358e97f.png',
	'https://www.natickma.gov/ImageRepository/Document?documentID=1251',
	'http://cdn.cnn.com/cnnnext/dam/assets/190910120101-04-shopping-cities-photos.jpg',
	'https://www.t-systems.com/resource/image/159554/ratio3x4/768/1024/7cdefba5640b2a18ea3d34adc6009e06/rZ/m-smart-city.jpg',
	'https://www.brookings.edu/wp-content/uploads/2016/04/newyork_skyline_modern_city.jpg',
];

export { FILL_1, FILL_2 };
