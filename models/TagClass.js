/**
 * @id - string
 * @title - string
 * @active - boolean
 */
class TagClass {
	constructor(id, title, active) {
		this.id = id;
		this.title = title;
		this.active = active;
	}
}

class SuperTagClass {
	constructor(id, title, active) {
		this.id = id;
		this.title = title;
		this.active = active;
	}
}

class SubTagClass {
	constructor(id, title, active) {
		this.id = id;
		this.title = title;
		this.active = active;
	}
}

export { TagClass, SuperTagClass, SubTagClass };
