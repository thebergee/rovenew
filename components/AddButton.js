/** @format */

import React, { Component } from 'react';
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native';

import tcolors from '../constants/ThemeColors';

import { Feather } from '@expo/vector-icons';

const w = 80;

export default class AddButton extends Component {
	render() {
		return (
			<TouchableOpacity
				activeOpacity={0.6}
				onPress={this.props.onpress}
				style={styles.addButton}
			>
				<View
				style={[styles.circle,
					{ backgroundColor: this.props.bckColor },
				]}>
					<Feather name="plus" size={55} color="white" />
				</View>
			</TouchableOpacity>
		);
	}
}

AddButton.defaultProps = {
	bckColor: tcolors.BckgdColor,//#0A89C2
	onpress: console.log(' was pressed'),
};

const styles = StyleSheet.create({
	//KEEP
	addButton: {
		zIndex: 100,
		//backgroundColor:'plum',
	},
	circle: {
		width: w,
		height: w,
		borderRadius: w / 2,
		overflow: 'hidden',
		alignContent: 'center',
		justifyContent: 'center',
		alignItems: 'center'
	},
});
