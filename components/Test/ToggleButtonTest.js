import React from 'react';
import { Image, Button, Alert, View, Text,TouchableHighlight,Pressable } from 'react-native';

import tcolors from '../../constants/ThemeColors';
import def from '../../constants/DefaultStyles';
import clock from '../../constants/ClockStyles';

//https://reactjs.org/docs/handling-events.html

class Toggle extends React.Component {
	constructor(props) {
		super(props);
		this.state = { isToggleOn: false };

		// This binding is necessary to make `this` work in the callback
		this.handleClick = this.handleClick.bind(this);
	}

	componentDidMount() {
		//console.log('\nMounting Toggle button')
		//console.log('isToggleOn: '+this.state.isToggleOn)
	}

	componentWillUnmount() {
		//console.log('Unmounting Toggle button')
		//console.log('isToggleOn: '+this.state.isToggleOn)
	}

	handleClick() {
		this.setState((state) => ({
			isToggleOn: !state.isToggleOn,
		}));
		//console.log('isToggleOn:' +this.state.isToggleOn)
	}

	render() {
		return (
			<Pressable style onPress={this.handleClick}
			style={[
				clock.buttonA,
				{
					//(condition) ? (true run this code) : (false run this code)
					backgroundColor: this.state.isToggleOn
						? tcolors.secondaryB:tcolors.secondaryC

				},
			]}>
				<Text style={clock.buttonText}>{this.state.isToggleOn ? 'ON' : 'OFF'}</Text>
			</Pressable>
		);
	}
}
export default Toggle;
