import React from 'react';
import { Image, Button, Alert, View, Text } from 'react-native';
//https://reactjs.org/docs/state-and-lifecycle.html

import clock from '../../constants/ClockStyles';

class Clock extends React.Component {
	constructor(props) {
		super(props);
		this.state = { date: new Date() };
	}

	componentDidMount() {
		//console.log('component Clock mounted')
		this.timerID = setInterval(() => this.tick(), 1000);
	}

	componentWillUnmount() {
		clearInterval(this.timerID);
		//console.log('component Clock unmounted')
	}

	tick() {
		this.setState({
			date: new Date(),
		});
	}

	render() {
		return (
			<View style={clock.circle}>
				<Text style={clock.mainText}>
					{this.state.date.toLocaleTimeString()}
				</Text>
			</View>
		);
	}
}

export default Clock;
