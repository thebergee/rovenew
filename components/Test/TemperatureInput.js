import React, { useState } from 'react';
import { Text, View, TextInput, Button, Alert } from 'react-native';

import clock from '../../constants/ClockStyles';
//https://reactjs.org/docs/lifting-state-up.html

const scaleNames = {
	c: 'Celsius',
	f: 'Fahrenheit'
  };

export default class TemperatureInput extends React.Component {
	constructor(props) {
		super(props);
		this.handleChange = this.handleChange.bind(this);
		//this.state = {temperature: ''};
	}

	/* Send changes to the parent */
	handleChange(e) {
		//this.setState({temperature: e});
		this.props.onTemperatureChange(e);
	}

	/* Use READ-ONLY Props instead of State */
	render() {
		//const temperature = this.state.temperature;
		//const scale = this.props.scale;

		const temperature = this.props.temperature;
		const scale = this.props.scale;
		return (
			<View style={clock.tempContainer}>
				<Text style={clock.bodyText}>
					Enter temperature in {scaleNames[scale]}:
				</Text>
				<TextInput
					style={clock.tempInput}
					value={temperature}
					maxLength={3}
					onChangeText={this.handleChange}
					autoCorrect={false}
					keyboardType="number-pad"
				/>
			</View>
		);
	}
}
