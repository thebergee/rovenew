import React, { Component, useState } from 'react';
import { Text, View, TextInput, Button, Alert } from 'react-native';

import clock from '../../constants/ClockStyles';
//https://reactjs.org/docs/forms.html

export default class NameForm extends Component {
	constructor(props) {
		super(props);
		this.state = { value: '' };

		this.handleChange = this.handleChange.bind(this);
		this.handleSubmit = this.handleSubmit.bind(this);
	}

	handleChange(e) {
		this.setState({ value: e.target.value });
	}

	handleSubmit(e) {
		alert('A name was submitted: ' + this.state.value);
		e.preventDefault();
	}

	render() {
		return (
			<View>
				<View style={{flexDirection:'row'}}>
					<Text style={clock.bodyText}>Name: </Text>
					<TextInput
						style={clock.inputName} // Inherit any props passed to it; e.g., multiline, numberOfLines below
						editable
					/>
				</View>
				<Button
					title="submit"
					onPress={this.handleSubmit}
				/>
			</View>
		);
	}
}
