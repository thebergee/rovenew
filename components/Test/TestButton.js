import React, { Component } from 'react';
import { Text, View, TouchableOpacity, StyleSheet } from 'react-native';

export default class TestButton extends Component {
	render() {
		return (
			<View>
				<TouchableOpacity
					style={[
						styles.buttonTest,
						{ backgroundColor: this.props.bckColor },
					]}
					onPress={this.props.onpress}
				>
					<Text style={styles.buttonText}>{this.props.title}</Text>
				</TouchableOpacity>
			</View>
		);
	}
}

TestButton.defaultProps = {
	title: 'some_title',
	bckColor: 'royalblue',
	onpress: console.log(' was pressed'),
};

const styles = StyleSheet.create({
	buttonTest: {
		paddingVertical: 10,
		//paddingHorizontal: 15,
		paddingLeft: 17,
		paddingRight: 17,

		marginTop: 8.5,
		marginBottom: 8.5,
		marginHorizontal: 4,

		borderRadius: 40,
		overflow: 'hidden',

		alignContent: 'center',
	},
	
	buttonText: {
		fontFamily: 'monMed',
		fontSize: 18,
		color: '#fff',
		textAlign: 'center',
	},
});
