/** @format */

import React, { Component } from 'react';
import { Text, StyleSheet, View, TouchableOpacity } from 'react-native';

import tcolors from '../../constants/ThemeColors';
import def from '../../constants/DefaultStyles';
import tagdef from '../../constants/TagStyles';

import SubTagClass from '../../models/TagClass';

console.log('setting up the substags\n');

class SubTagButton extends Component {
	constructor(props) {
		super(props);
		this.state = { status: false, isActive: 0 };

		this.updateStatus = this.updateStatus.bind(this);
	}

	componentDidMount() {
		//console.log('MOUNTED component: '+this.props.title+' : '+this.props.id+' : '+this.state.status)
	}

	componentWillUnmount() {
		console.log(
			'UNMOUNTED component: ' +
				this.props.title +
				' : ' +
				this.props.id +
				' : ' +
				this.state.status
		);
	}

	//https://stackoverflow.com/questions/41446560/react-setstate-not-updating-state
	updateStatus() {
		this.setState(({ status }) => ({ status: !status}),() => {console.log(this.props.title + ': ' + this.state.status)});
	}

	render() {
		return (
			<TouchableOpacity
				onPress={() => this.updateStatus()}
				style={[
					tagdef.button,
					{
						//(condition) ? (true run this code) : (false run this code)
						backgroundColor: this.state.status
							? tcolors.secondaryA
							: tcolors.secondaryC,
					},
				]}
			>
				<Text style={tagdef.buttonText}>{this.props.title}</Text>
			</TouchableOpacity>
		);
	}
}

SubTagButton.defaultProps = {
	id: 'someid',
	title: 'some_title',
};

export default SubTagButton;
