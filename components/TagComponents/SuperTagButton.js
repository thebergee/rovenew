import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Text, StyleSheet, View, TouchableOpacity } from 'react-native';

import tcolors from '../../constants/ThemeColors';
import def from '../../constants/DefaultStyles';
import tagdef from '../../constants/TagStyles';

//modified from https://www.positronx.io/create-radio-button-component-in-react-native/

class SuperTagButton extends Component {
	state = {
		id: 'sp1', //inital value is 'other' id: sp1
	};

	componentDidMount() {
		//console.log('MOUNTED component: '+this.props.title+' : '+this.props.id+' : '+this.state.status)
	}

	componentWillUnmount() {
		console.log(
			"\n"+
			'UNMOUNTED SUPER TAG component: ' +
				this.state.title +
				' : ' +
				this.state.id
		);
	}

	render() {
		const { PROP } = this.props;
		const { id } = this.state;

		return (
			<View style={tagdef.tagsContainer}>
				{PROP.map((res) => {
					return (
						<View id={res.id} key={res.id}>
							<TouchableOpacity
								style={[
									tagdef.superbutton,
									{
										backgroundColor:
											id === res.id && this.state.id
												? tcolors.secondaryA
												: tcolors.secondaryC,
									},
								]}
								onPress={() => {
									this.setState({
										title: res.title,
										id: res.id,
									});
								}}
							>
								<Text style={tagdef.buttonText}>{res.title}</Text>
							</TouchableOpacity>
						</View>
					);
				})}
			</View>
		);
	}
}

SuperTagButton.defaultProps = {
	id: 'someid',
	title: 'some_title',
};

export default SuperTagButton;
