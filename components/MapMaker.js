/** @format */

import React, { Component } from 'react';
import { Text, StyleSheet, View, TouchableOpacity } from 'react-native';

import tcolors from '../constants/ThemeColors';
import def from '../constants/DefaultStyles';
import tagdef from '../constants/TagStyles';

console.log('setting up the substags\n');

class MapMarker extends Component {
	constructor(props) {
		super(props);
		this.state = { 
            superId: 'sp1'
        };

		this.updateStatus = this.updateStatus.bind(this);
	}

	componentDidMount() {
		//console.log('MOUNTED component: '+this.props.title+' : '+this.props.id+' : '+this.state.status)
	}

	componentWillUnmount() {
		console.log(
			'UNMOUNTED component: ' +
				this.props.title +
				' : ' +
				this.props.id +
				' : ' +
				this.state.status
		);
	}

	//https://stackoverflow.com/questions/41446560/react-setstate-not-updating-state
	updateStatus() {
		this.setState(({ status }) => ({ status: !status}),() => {console.log(this.props.title + ': ' + this.state.status)});
	}

	render() {
		return (
			<TouchableOpacity
				onPress={() => this.updateStatus()}
				style={[
					tagdef.button,
					{
						//(condition) ? (true run this code) : (false run this code)
						backgroundColor: this.state.status
							? tcolors.secondaryA
							: tcolors.secondaryC,
					},
				]}
			>
				<Text style={tagdef.buttonText}>{this.props.title}</Text>
			</TouchableOpacity>
		);
	}
}

MapMarker.defaultProps = {
	superId: 'sp1'
};

export default MapMarker;
