import React, { Component } from 'react';
import { Text, View, ScrollView, Image, FlatList } from 'react-native';

import TestButton from '../../components/Test/TestButton';
import AddButton from '../../components/AddButton';

import { FILL_1, FILL_2 } from '../../data/GalleryFill';

import def from '../../constants/DefaultStyles';
import imgsty from '../../constants/ImagePicStyles';

import tcolors from '../../constants/ThemeColors';

export default class ImagePicScreen_Test extends Component {
	//Renders the images using map
	RenderGalleryFill() {
		return FILL_2.map((itemData) => {
			return (
				<Image
					source={{ uri: itemData }}
					style={imgsty.imageFrame}
					resizeMode="cover"
				/>
			);
		});
	}
	//https://stackoverflow.com/questions/50920628/difference-between-flatlist-and-virtualizedlist

	FlatListGalleryFillB() {
		return (
			<Image
				source={{ uri: itemData }}
				style={imgsty.imageFrame}
				resizeMode="cover"
			/>
		);
	}

	addImageContainer() {
		return (
			<View style={imgsty.addImageContainer}>
				<AddButton
					bckColor={tcolors.galleryAdd}
					onpress={() => console.log('add picture')}
				/>
			</View>
		);
	}

	render() {
		return (
			<View style={def.screen}>
				<View style={def.testButtonContainer}>
					<ScrollView horizontal={true}>
						<TestButton
							title="Home"
							onpress={() =>
								this.props.navigation.navigate('Home')
							}
							bckColor="mediumvioletred"
						/>
						<TestButton
							title="Clock Test"
							onpress={() =>
								this.props.navigation.navigate('Clock_Test')
							}
						/>
						<TestButton
							title="Gallery Test"
							onpress={() =>
								props.navigation.navigate('Gallery_Test')
							}
						/>
					</ScrollView>
				</View>
				<ScrollView>
					<View style={imgsty.galleryScreen}>
						<this.addImageContainer/>
						<this.RenderGalleryFill />
					</View>
					</ScrollView>
			</View>
		);
	}
}
