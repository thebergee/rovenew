import React, { Component } from 'react';
import { Text, View, ScrollView, Alert } from 'react-native';

import TestButton from '../../components/Test/TestButton';

import Clock from '../../components/Test/Clock';
import Toggle from '../../components/Test/ToggleButtonTest';
import NameForm from '../../components/Test/NameForm';
import TemperatureInput from '../../components/Test/TemperatureInput';
import TempCalculator from '../../components/Test/TempCalculator';

import clock from '../../constants/ClockStyles';
import def from '../../constants/DefaultStyles';

const ClockScreen_Test = (props, navigate) => {
	return (
		<View style={clock.screen}>
			<View style={def.testButtonContainer}>
				<ScrollView horizontal={true}>
					<TestButton
						title="Home"
						onpress={() => props.navigation.navigate('Home')}
						bckColor="mediumvioletred"
					/>
					<TestButton
						title="Hello"
						onpress={() =>
							Alert.alert(
								'Alert Title',
								'My Alert Msg',
								[
									{
										text: 'OK',
										onPress: () =>
											console.log('OK Pressed'),
									},
								],
								{ cancelable: false }
							)
						}
					/>
					<TestButton
						title="Image Picker Test"
						onpress={() => props.navigation.navigate('Image_Test')}
					/>
					<TestButton
							title="Gallery Test"
							onpress={() =>
								props.navigation.navigate('Gallery_Test')
							}
						/>
				</ScrollView>
			</View>
			<ScrollView style={clock.screen}>
				<View
					style={clock.itemContainer}
				>
					<Clock />
				</View>
				<View
					style={clock.itemContainer}
				>
					<Toggle title="soomething" />
				</View>
				<View style={clock.itemContainer}>
					<NameForm value="somdfkljsdlkfjdslk" />
				</View>
				<View style={clock.itemContainer}>
					<TempCalculator />
				</View>
			</ScrollView>
		</View>
	);
};
export default ClockScreen_Test;
