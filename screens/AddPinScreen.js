/** @format */

import React from 'react';
import { View, Dimensions, ScrollView } from 'react-native';
import MapView, { Marker } from 'react-native-maps';

import def from '../constants/DefaultStyles';

import defaultPin from '../assets/PinAssets/DefaultPin.png';

import TestButton from '../components/Test/TestButton';

const { width, height } = Dimensions.get('window');

const ASPECT_RATIO = width / height;
const LATITUDE = 37.78825;
const LONGITUDE = -122.4324;
const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

export default class AddPinScreen extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			region: {
				latitude: LATITUDE,
				longitude: LONGITUDE,
				latitudeDelta: LATITUDE_DELTA,
				longitudeDelta: LONGITUDE_DELTA,
			},
			marker: {
				coordinate: undefined,
			},
		};
		this.onMapPress = this.onMapPress.bind(this);
	}

	onMapPress(e) {
		this.setState({
			marker: {
				coordinate: e.nativeEvent.coordinate,
			},
		});
	}

	makerAdded()
	{
		if(this.state.marker.coordinate==undefined)
		{
			console.log('makerAdded: False')
			return false
		}
		else {
			console.log('makerAdded: True')
			return true
		}
	}

	render() {
		return (
			<View View style={def.screen}>
				<View style={def.testButtonContainer}>
					<ScrollView horizontal={true}>
						<TestButton
							title={'Print coordinates'}
							onpress={() => {
								console.log(this.state.marker.coordinate)
							}}
						/>
						<TestButton
							title={'Maker Added'}
							onpress={() => {
								this.makerAdded()
							}}
						/>
					</ScrollView>
				</View>
				<MapView
					provider={this.props.provider}
					style={def.map}
					initialRegion={{
						latitude: LATITUDE,
						longitude: LONGITUDE,
						latitudeDelta: LATITUDE_DELTA,
						longitudeDelta: LONGITUDE_DELTA,
					}}
					onLongPress={this.onMapPress}
				>
					<Marker
						image={defaultPin}
						coordinate={this.state.marker.coordinate}
						onPress={() => {
							this.props.navigation.navigate('AddTags', {
							});
						}}
					/>
				</MapView>
			</View>
		);
	}
}
