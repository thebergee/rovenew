/** @format */

import React from 'react';
import { View,ScrollView } from 'react-native';

import AddButton from '../components/AddButton';

import imgsty from '../constants/ImagePicStyles';
import tcolors from '../constants/ThemeColors';

export default class GalleryScreen extends React.Component {

	addImageContainer() {
		return (
			<View style={imgsty.addImageContainer}>
				<AddButton
					bckColor={tcolors.galleryAdd}
					onpress={() => console.log('add picture')}
				/>
			</View>
		);
	}

	render() {
		return (
				<ScrollView style={imgsty.galleryScreen}>
					<this.addImageContainer/>
				</ScrollView>
		);
	}
}
