/** @format */

import React from 'react';
import { Text, View, ScrollView } from 'react-native';

import def from '../constants/DefaultStyles';
import tagdef from '../constants/TagStyles';

import { superTags, subTags } from '../data/Tags';

import SubTagButton from '../components/TagComponents/SubTagButton';
import SuperTagButton from '../components/TagComponents/SuperTagButton';
import TestButton from '../components/Test/TestButton';


export default class SearchTagsScreen extends React.Component {
	RenderButtonsSubPinsTest() {
		return subTags.map((itemData) => {
			return (
				<SubTagButton
					title={itemData.title}
					id={itemData.id}
					key={itemData.id}
				/>
			)
		});
	}

	render() {
		return (
			<View style={def.screen}>
				<ScrollView>
					<View style={tagdef.searchCon}>
						<View style={tagdef.buffer} />
						<Text style={tagdef.labels}>Location Types</Text>
						<SuperTagButton PROP={superTags} />
						<View style={tagdef.buffer} />
						<Text style={tagdef.labels}>Keywords</Text>
						<View style={tagdef.tagsContainer}>
							{this.RenderButtonsSubPinsTest()}
						</View>
						<View style={tagdef.buffer} />
					</View>
				</ScrollView>
			</View>
		);
	}
}
