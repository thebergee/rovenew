/** @format */

import React from 'react';
import { Text, View, ScrollView } from 'react-native';

import def from '../constants/DefaultStyles';
import tagdef from '../constants/TagStyles';

import { superTags, subTags } from '../data/Tags';

import SubTagButton from '../components/TagComponents/SubTagButton';
import SuperTagButton from '../components/TagComponents/SuperTagButton';
import TestButton from '../components/Test/TestButton';

export default class AddTagsScreen extends React.Component {
	RenderButtonsSubPinsTest() {
		return subTags.map((tagData) => {
			return (
				<SubTagButton
					title={tagData.title}
					id={tagData.id}
					key={tagData.id}
				/>
			);
		});
	}
	//makerCoord = route.params; //color and name is used in the navigator
	//enables params to be passed in from categories screen to categoryMeal sceen, and to the navigator

	makerCoordinates() {
		console.log('not implemented');
	}
	setSubPins() {
		console.log('to be implemented: Set Sub Pins');
	}

	printSubPins() {
		console.log('to be implemented: Pirnt Sub Pins');
		console.log(subTags);
	}

	render() {
		return (
			<View style={def.screen}>
				<ScrollView>
					<View style={tagdef.searchCon}>
						<View style={tagdef.buffer} />
						<Text style={tagdef.labels}>Location Types</Text>
						<SuperTagButton PROP={superTags} />
						<View style={tagdef.buffer} />
						<Text style={tagdef.labels}>Keywords</Text>
						<View style={tagdef.tagsContainer}>
							{this.RenderButtonsSubPinsTest()}
						</View>
						<View style={tagdef.buffer} />
					</View>
					<View style={def.testButtonContainer}>
						<ScrollView horizontal={true}>
							<TestButton
								title={'Maker Coordinates'}
								onpress={() => {
									this.makerCoordinates();
								}}
							/>
							<TestButton
								title={'set Super and Sub'}
								onpress={() => {
									this.setSubPins();
								}}
							/>
							<TestButton
								title={'print Super and Sub'}
								onpress={() => {
									this.printSubPins();
								}}
							/>
						</ScrollView>
					</View>
				</ScrollView>
			</View>
		);
	}
}

var superPinActive_S = [0, 0, 0, 0, 0];
var superPinArray_S = [
	{
		title: 'Other',
		id: 'sp1',
		active: false,
	},
	{
		//Landmark
		title: 'City',
		id: 'sp2',
		active: false,
	},
	{
		//Beach
		title: 'Rural',
		id: 'sp3',
		active: false,
	},
	{
		//Graffiti
		title: 'Coastal',
		id: 'sp4',
		active: false,
	},
	{
		//WildLife
		title: 'Wilderness',
		id: 'sp5',
		active: false,
	},
];

var subPinsActive_S = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
var subPinArray_S = [
	{
		//Architecture
		title: 'Architecture',
		id: 'sb1',
		active: false,
	},
	{
		//Landmark
		title: 'Landmark',
		id: 'sb2',
		active: false,
	},
	{
		//Beach
		title: 'Beach',
		id: 'sb3',
		active: false,
	},
	{
		//Graffiti
		title: 'Graffiti',
		id: 'sb4',
		active: false,
	},
	{
		//WildLife
		title: 'WildLife',
		id: 'sb5',
		active: false,
	},
	{
		//Transport
		title: 'Transport',
		id: 'sb6',
		active: false,
	},
	{
		//Abandoned
		title: 'Abandoned',
		id: 'sb7',
		active: false,
	},
	{
		//Entertainment
		title: 'Entertainment',
		id: 'sb8',
		active: false,
	},
	{
		//Commercial
		title: 'Commercial',
		id: 'sb9',
		active: false,
	},
	{
		//Residential
		title: 'Residential',
		id: 'sb10',
		active: false,
	},
];
