/** @format */

import React from 'react';
import { View, ScrollView, Dimensions } from 'react-native';
import MapView from 'react-native-maps';

import AddButton from '../components/AddButton';
import TestButton from '../components/Test/TestButton';

import def from '../constants/DefaultStyles';

const windowWidth = Dimensions.get('screen').width;
const windowHeight = Dimensions.get('screen').height;

const ASPECT_RATIO = windowWidth / windowHeight;
const LATITUDE = 37.78825;
const LONGITUDE = -122.4324;
const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

export default class HomeScreen extends React.Component {
	render() {
		return (
			<View style={def.screen}>
				<View style={def.testButtonContainer}>
					<ScrollView horizontal={true}>
						<TestButton
							title="Gallery"
							onpress={() =>
								this.props.navigation.navigate('Gallery')
							}
						/>
						<TestButton
							title="Clock Test"
							onpress={() =>
								this.props.navigation.navigate('Clock_Test')
							}
						/>
						<TestButton
							title="Image Picker Test"
							onpress={() =>
								this.props.navigation.navigate('Image_Test')
							}
						/>
						<TestButton
							title="Gallery Test"
							onpress={() =>
								this.props.navigation.navigate('Gallery_Test')
							}
						/>
					</ScrollView>
				</View>
				<MapView
					provider={this.props.provider}
					style={def.map}
					initialRegion={{
						latitude: LATITUDE,
						longitude: LONGITUDE,
						latitudeDelta: LATITUDE_DELTA,
						longitudeDelta: LONGITUDE_DELTA,
					}}
				></MapView>

				<View style={def.addButtonContainer}>
					<AddButton
						onpress={() =>
							this.props.navigation.navigate('AddPin')
						}
					/>
				</View>
			</View>
		);
	}
}
